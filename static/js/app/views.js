var MainPageDemotivators = Backbone.View.extend({
    template: _.template('<% _.each(demotivators, function(demotivator) { %> <li><%= demotivator.title %></li><%}); %>'),

    tagName: 'ul',

    demotivators: undefined,
    initialize: function() {
        this.demotivators = new Demotivators();
        this.demotivators.fetch();
    },

    render: function() {
        this.$el.html(
            this.template(
                {
                    demotivators: this.demotivators.toJSON()
                }
            )
        );
        return this;
    }
});

var MainView = Backbone.View.extend({
    events: {
        'click a': 'navigate'
    },

    navigate: function(event) {
        event.preventDefault();
        var link = event.currentTarget;
        var url = link.href;
        url = RemoveBaseUrl(url);
        app.navigate(url, {trigger: true});
    },

    initialize: function() {
        this.setElement($('#content'));
        $('#modal').fadeOut();
        $('#hat').removeClass('modal', 1000);
    },

    render: function() {
        var demotivators = new MainPageDemotivators();
        demotivators.render()
        this.$el.empty().append(demotivators.$el);
    }
});

var ModalView = Backbone.View.extend({
    initialize: function() {
        var modal = $('#modal');
        this.setElement(modal);
        var hat = $('#hat');
        hat.addClass('modal', 1000, function() {
            modal.fadeIn(function() {
                modal.find('#window-content').slideUp(function() {
                    $(this).slideDown();
                });
            });
        });
    }
});