var DewoteRouter = Backbone.Router.extend({
    routes: {
        '': 'main',
        'modal': 'modal'
    },

    main: function() {
        var main_view = new MainView();
        main_view.render();
    },

    modal: function() {
        var modal_view = new ModalView();
    }
});

$(function() {
    window.app = new DewoteRouter();
    Backbone.history.start({pushState: true, root: '/'});
});