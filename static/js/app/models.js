var Demotivator = Backbone.Model.extend({
    urlRoot: '/api/demotivator'
});

var Demotivators = Backbone.Collection.extend({
    model: Demotivator,
    url: '/api/demotivator'
});