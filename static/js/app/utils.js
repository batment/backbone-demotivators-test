function RemoveBaseUrl(url) {
    /*
     * Replace base URL in given string, if it exists, and return the result.
     *
     * e.g. "http://localhost:8000/api/v1/blah/" becomes "/api/v1/blah/"
     *      "/api/v1/blah/" stays "/api/v1/blah/"
     */

    if(!url || url.length == 0) {
        return '/';
    }

    var baseUrlPattern = /^https?:\/\/[^/]+/;
    var result = "";

    var match = baseUrlPattern.exec(url);
    if (match != null) {
        result = match[0];
    }

    if (result.length > 0) {
        url = url.replace(result, "");
    }

    if (url[0] != '/') {
        url = '/' + url;
    }

    return url;
}

//
//window.TastypieModel = Backbone.Model.extend({
//    base_url: function() {
//      var temp_url = Backbone.Model.prototype.url.call(this);
//      return (temp_url.charAt(temp_url.length - 1) == '/' ? temp_url : temp_url+'/');
//    },
//
//    url: function() {
//      return this.base_url();
//    }
//});
//
//
//window.TastypieCollection = Backbone.Collection.extend({
//    parse: function(response) {
//        this.recent_meta = response.meta || {};
//        return response.objects || response;
//    }
//});

